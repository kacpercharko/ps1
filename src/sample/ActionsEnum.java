package sample;

public enum ActionsEnum {
    NULL,
    HAND,
    LINE,
    SQUARE,
    ELLIPSE;
}
