package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class ModalController implements MyModal {
    @FXML
    TextField input1, input2, input3, input4;

    @Override
    public double getInput1() {
        return Double.parseDouble(input1.getCharacters().toString());
    }

    @Override
    public double getInput2() {
        return Double.parseDouble(input2.getCharacters().toString());
    }

    @Override
    public double getInput3() {
        return Double.parseDouble(input3.getCharacters().toString());
    }

    @Override
    public double getInput4() {
        return Double.parseDouble(input4.getCharacters().toString());
    }

    @Override
    public void setInput1(double value) {
        input1.appendText(Double.toString(value));
    }

    @Override
    public void setInput2(double value) {
        input2.appendText(Double.toString(value));
    }

    @Override
    public void setInput3(double value) {
        input3.appendText(Double.toString(value));
    }

    @Override
    public void setInput4(double value) {
        input4.appendText(Double.toString(value));
    }


}
