package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;

import static sample.ActionsEnum.*;

public class Controller {


    @FXML
    ColorPicker colorPicker;
    @FXML
    private Group group;

    Color color = Color.BLACK;
    ActionsEnum actionsEnum = HAND;
    ActionsEnum previousState = NULL;
    int clickCount = 0;

    Line lineHelper;
    Rectangle rectangleHelper;
    private Ellipse ellipseHelper;

    double lastClickedX;
    double lastClickedY;
    //    private double dragX;
//    private double dragY;
    double dragXDif;
    double dragYdif;
    double dragX2Dif;
    double dragY2dif;
    boolean dragged = false;
    boolean edit = false;


    @FXML
    public void handleLiniaButtonClick(ActionEvent actionEvent) {
        edit = false;
        clickCount = 0;
        actionsEnum = LINE;
    }


    @FXML
    public void handleHandButtonClick(ActionEvent actionEvent) {
        edit = false;
        clickCount = 0;
        actionsEnum = HAND;
    }

    @FXML
    public void handleSquareButtonClick(ActionEvent actionEvent) {
        edit = false;
        clickCount = 0;
        actionsEnum = SQUARE;
    }

    public void handleEllipseButton(ActionEvent actionEvent) {
        edit = false;
        actionsEnum = ELLIPSE;
        clickCount = 0;
    }

    public void handleCustomButtonClick(ActionEvent actionEvent) throws IOException {
        if(actionsEnum != NULL) {
            Shape shape = null;
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getModalByActionType(actionsEnum));
            DialogPane myDialog = fxmlLoader.load();
            MyModal controller = fxmlLoader.getController();
            printShape(shape, myDialog, controller);
        }
    }

    private URL getModalByActionType(ActionsEnum actionsEnum) {
        switch (actionsEnum) {
            case LINE:
                return getClass().getResource("line.fxml");
            case SQUARE:
                return getClass().getResource("square.fxml");
            case ELLIPSE:
                return getClass().getResource("ellipse.fxml");
        }
        return null;
    }

    @FXML
    public void handleAnchorClick(MouseEvent mouseEvent) {
        lastClickedY = mouseEvent.getY();
        lastClickedX = mouseEvent.getX();
        System.out.println(actionsEnum.name());
        System.out.println(edit);
        switch (actionsEnum) {
            case NULL:
                break;
            case LINE:
                processLineDrawing(mouseEvent.getX(), mouseEvent.getY());
                break;
            case SQUARE:
                processSquareDrawing(mouseEvent.getX(), mouseEvent.getY());
                break;
            case ELLIPSE:
                processEllipseDrawing(mouseEvent.getX(), mouseEvent.getY());
                break;
        }
    }

    private void processEllipseDrawing(double x, double y) {
        if (clickCount == 0 && actionsEnum == ELLIPSE) {
            Ellipse ellipse = new Ellipse(x, y, 2, 2);
            ellipse.setFill(color);
            ellipseHelper = ellipse;
            clickCount++;
            group.getChildren().add(ellipse);

            setElipseEventListeners(ellipse);

        } else if (clickCount == 1 && actionsEnum == ELLIPSE) {
            clickCount = 0;
        }

    }

    private void processSquareDrawing(double x, double y) {
        if (clickCount == 0 && actionsEnum == SQUARE) {
            Rectangle rectangle = new Rectangle(x, y, 1, 1);
            rectangle.setFill(color);
            rectangleHelper = rectangle;
            setOnRectangleEventListeners(rectangle);
            clickCount++;
            group.getChildren().add(rectangle);
        } else if (clickCount == 1 && actionsEnum == SQUARE) {
            clickCount = 0;
        }


    }

    private void processLineDrawing(double x, double y) {
        if (clickCount == 0 && actionsEnum == LINE) {
            Line line = new Line(x, y, x, y);
            lineHelper = line;
            line.setStrokeWidth(10);
            line.setStroke(color);
            clickCount++;
            group.getChildren().add(line);
            setLineEventListeners(line);
        } else if (clickCount == 1) {
            clickCount = 0;
            lineHelper = Utils.swapLinePointsIfRequired(lineHelper);
        }
    }

    public void handleMouseMove(MouseEvent mouseEvent) {
        double x = mouseEvent.getX();
        double y = mouseEvent.getY();

        if (actionsEnum == LINE && clickCount == 1) {
            lineHelper.setEndX(x);
            lineHelper.setEndY(y);
        }
        if (actionsEnum == SQUARE && clickCount == 1) {
            boolean isGoingUp = lastClickedY > y ? true : false;
            boolean isGoingLeft = lastClickedX < x ? true : false;
            if (isGoingLeft) {
                rectangleHelper.setWidth(x - rectangleHelper.getX());
            } else {
                rectangleHelper.setX(x);
                rectangleHelper.setWidth(lastClickedX - x);
            }
            if (isGoingUp) {
                rectangleHelper.setY(y);
                rectangleHelper.setHeight(lastClickedY - y);
            } else {
                rectangleHelper.setHeight(y - rectangleHelper.getY());
            }
        }
        if (actionsEnum == ELLIPSE && clickCount == 1) {
            ellipseHelper.setCenterX(x);
            ellipseHelper.setRadiusX(Math.abs(lastClickedX - x));
            ellipseHelper.setCenterY(y);
            ellipseHelper.setRadiusY(Math.abs(lastClickedY - y));
        }


    }

    @FXML
    public void handleSetColor(ActionEvent actionEvent) {
        color = colorPicker.getValue();
    }


    public void handleOnReleased(MouseEvent mouseEvent) {
        dragged = false;
    }

    private void setLineEventListeners(Line line) {
        line.setOnDragDetected(mouseEvent -> {
            dragged = true;
            dragXDif = line.getStartX() - mouseEvent.getX();
            dragYdif = line.getStartY() - mouseEvent.getY();
            dragX2Dif = line.getEndX() - mouseEvent.getX();
            dragY2dif = line.getEndY() - mouseEvent.getY();

        });
        line.setOnMouseDragged(mouseEvent -> {
            if (actionsEnum == HAND && dragged) {
                double positionX = mouseEvent.getX();
                double positionY = mouseEvent.getY();
                line.setStartX(positionX + dragXDif);
                line.setStartY(positionY + dragYdif);
                line.setEndX(positionX + dragX2Dif);
                line.setEndY(positionY + dragY2dif);
            }
            if (edit && dragged) {
                double positionX = mouseEvent.getX();
                double positionY = mouseEvent.getY();
                line.setEndX(positionX + dragX2Dif);
                line.setEndY(positionY + dragY2dif);
            }
        });
        line.setOnMouseClicked(mouseEvent -> {
            if(edit) {
                try {
                    editShape(line, line.getStartX(), line.getStartY(), line.getEndX(), line.getEndY());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setElipseEventListeners(Ellipse ellipse) {
        ellipse.setOnDragDetected(mouseEvent -> {
            dragged = true;
            dragXDif = ellipse.getCenterX() - mouseEvent.getX();
            dragYdif = ellipse.getCenterY() - mouseEvent.getY();

        });

        ellipse.setOnMouseDragged(mouseEvent -> {
            if (actionsEnum == HAND && dragged) {
                double positionX = mouseEvent.getX();
                double positionY = mouseEvent.getY();
                ellipse.setCenterX(positionX + dragXDif);
                ellipse.setCenterY(positionY + dragYdif);

            }
            if (edit && dragged) {
                double positionX = mouseEvent.getX();
                double positionY = mouseEvent.getY();
                ellipse.setRadiusX(Math.abs(positionX - ellipse.getCenterX()));
                ellipse.setRadiusY(Math.abs(positionY - ellipse.getCenterY()));
            }
        });
        ellipse.setOnMouseClicked(mouseEvent -> {
            if(edit) {
                try {
                    editShape(ellipse, ellipse.getCenterX(), ellipse.getCenterY(), ellipse.getRadiusX(), ellipse.getRadiusY());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setOnRectangleEventListeners(Rectangle rectangle) {
        rectangle.setOnDragDetected(mouseEvent -> {
            dragged = true;
            dragXDif = rectangle.getX() - mouseEvent.getX();
            dragYdif = rectangle.getY() - mouseEvent.getY();

        });
        rectangle.setOnMouseDragged(mouseEvent -> {
            if (actionsEnum == HAND && dragged) {
                double positionX = mouseEvent.getX();
                double positionY = mouseEvent.getY();
                rectangle.setX(positionX + dragXDif);
                rectangle.setY(positionY + dragYdif);

            }
            if (edit && dragged) {
                double positionX = mouseEvent.getX();
                double positionY = mouseEvent.getY();
                rectangle.setWidth(Math.abs(positionX - rectangle.getX()));
                rectangle.setHeight(Math.abs(positionY - rectangle.getY()));

            }
        });
        rectangle.setOnMouseClicked(mouseEvent -> {
            if(edit) {
                try {
                    editShape(rectangle, rectangle.getX(), rectangle.getY(), rectangle.getWidth(), rectangle.getHeight());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void handleEditClick(ActionEvent actionEvent) {
        edit = true;
        if (actionsEnum != NULL) {
            previousState = actionsEnum;
            actionsEnum = NULL;
        }
    }

    public void editShape(Shape shape, double val1, double val2, double val3, double val4) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getModalByActionType(previousState));
        DialogPane myDialog = fxmlLoader.load();
        MyModal controller = fxmlLoader.getController();
        controller.setInput1(val1); controller.setInput2(val2); controller.setInput3(val3); controller.setInput4(val4);
        printShape(shape, myDialog, controller);
    }

    private void printShape(Shape shape, DialogPane myDialog, MyModal controller) {
        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.setDialogPane(myDialog);
        dialog.setTitle("create " + actionsEnum.name());
        Optional<ButtonType> clickedButton = dialog.showAndWait();
        if (clickedButton.isPresent()) {
            if (clickedButton.get() == ButtonType.OK) {
                printFigure(shape,controller);
            }
        }
    }

    private void printFigure(Shape shape, MyModal controller) {
        group.getChildren().remove(shape);
        if (actionsEnum == LINE || shape instanceof Line) {
            shape = new Line(controller.getInput1(),controller.getInput2(),controller.getInput3(),controller.getInput4());
            setLineEventListeners((Line) shape);
            group.getChildren().add(shape);
        } else if (actionsEnum == SQUARE || shape instanceof Rectangle) {
            shape = new Rectangle(controller.getInput1(), controller.getInput2(),
                    controller.getInput3(), controller.getInput4());
            setOnRectangleEventListeners((Rectangle) shape);
            group.getChildren().add(shape);
        } else if (actionsEnum == ELLIPSE || shape instanceof Ellipse) {
            shape = new Ellipse(controller.getInput1(), controller.getInput2(),
                    controller.getInput3(), controller.getInput4());
            setElipseEventListeners((Ellipse) shape);
            group.getChildren().add(shape);
        }
        shape.setStrokeWidth(10);
        shape.setFill(color);
    }
}
