package sample;

public interface MyModal {
    public double getInput1();
    public double getInput2();
    public double getInput3();
    public double getInput4();
    public void setInput1(double value);
    public void setInput2(double value);
    public void setInput3(double value);
    public void setInput4(double value);
}
