package sample;

import javafx.scene.shape.Line;

public class Utils {

    public static Line swapLinePointsIfRequired(Line line){
        if(line.getStartX() > line.getEndX())
        {
            double startX = line.getStartX();
            double startY = line.getStartY();
            line.setStartX(line.getEndX()); line.setStartY(line.getEndY());
            line.setEndX(startX); line.setEndY(startY);
        }
        return line;
    }
}
